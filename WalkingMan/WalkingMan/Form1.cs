﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WalkingMan
{
    public partial class Form1 : Form
    {
        Image[] images = new Image[9];
        public Form1()
        {
            InitializeComponent();
            LoadImages();
        }

        void LoadImages()
        {
            for (int i = 0; i < 9; i++)
            {
                images[i] = Image.FromFile($@"..\..\Resources\{i}.png");
            }
        }

        void GoLeft()
        {
            Point p = pictureBox1.Location;
            p.X -= pictureBox1.Width;

            pictureBox1.Location = p;
        }

        void GoRight()
        {
            Point p = pictureBox1.Location;
            p.X += pictureBox1.Width;

            pictureBox1.Location = p;
        }

        int index = 0;
        void SetNextImage(bool isRight)
        {
            if (index > 8)
            {
                index = 0;
            }

            pictureBox1.Image = images[index];
            index++;

            if (isRight)
            {
                pictureBox1.Image.RotateFlip(RotateFlipType.Rotate180FlipY);
            }
        }

        void ResetImage(bool isRight)
        {
            pictureBox1.Image = images[0];
            if (isRight)
            {
                pictureBox1.Image.RotateFlip(RotateFlipType.Rotate180FlipY);
            }
        }

        bool isRight = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (isRight)
            {
                GoRight();
            }
            else
            {
                GoLeft();
            }

            if (pictureBox1.Location.X < 0 - pictureBox1.Width)
            {
                isRight = true;
                index = 0;
                //pictureBox1.Location = new Point(0, pictureBox1.Location.Y);
                GoRight();
            }

            if (pictureBox1.Location.X >= this.Width)
            {
                isRight = false;
                index = 0;
                //pictureBox1.Location = new Point(this.Width, pictureBox1.Location.Y);
                GoLeft();
            }

            SetNextImage(isRight);

        }
    }
}
